import path from "path";
import { ApolloServer } from 'apollo-server';
import { typeDefs } from './schema';
import { resolvers } from './resolvers/resolvers';
import { SpotifyApi, ISpotifyApi } from './datasources/spotify-api'
import { DataSources } from 'apollo-server-core/dist/graphqlOptions';

import * as dotenv from "dotenv";

dotenv.config({ path: path.resolve(__dirname + '/.dotenv') });


export interface MyDataSources {
  spotifyApi: ISpotifyApi
}
// the types may come from looking at https://github.com/apollographql/apollo-server/issues/2652
const server = new ApolloServer({
  typeDefs,
  resolvers,
  dataSources: (): DataSources<MyDataSources> => ({
    spotifyApi: new SpotifyApi(),
  }),
});

server.listen().then(() => {
  console.log(`
    🚀  Server is running!
    🔉  Listening on port 4000
    📭  Query at https://studio.apollographql.com/dev
  `);
});