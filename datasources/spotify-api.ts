import { RequestOptions, RESTDataSource } from "apollo-datasource-rest";
import axios, { AxiosRequestConfig } from "axios";
import { Spotify } from '../@types/SpotifyObjects'

export interface ISpotifyApi {
  getPlaylist: (playlistId: string) => Promise<Spotify.PlaylistObject>
  getPlaylistTracks: (playlistId: string, forUrl: boolean) => Promise<Spotify.PagingObject<Spotify.PlaylistTrackObject>>
  getTrack: (trackId: string) => Promise<Spotify.TrackObject>
  getAudioFeatures: (trackId: string) => Promise<Spotify.AudioFeaturesObject>
}

export class SpotifyApi extends RESTDataSource implements ISpotifyApi {
  access_token: string;
  token_expiration: number;

  constructor() {
    super();
    this.baseURL = "https://api.spotify.com/v1/";

    this.access_token = "";
    this.token_expiration = 0;
  }

  willSendRequest(request: RequestOptions) {
    console.log("Date now", Date.now());
    console.log("Token expires at: ", this.token_expiration);
    request.headers.set('Authorization', `Bearer ${this.access_token}`);
  }

  async requestToken() {
    if (this.token_expiration >= Date.now() && this.access_token !== "") {
      return;
    }
    const SpotifyTokenEndpoint = 'https://accounts.spotify.com/api/token'
    const authOptions: AxiosRequestConfig = {
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      },
      auth: {
        username: process.env.CLIENT_ID!,
        password: process.env.CLIENT_SECRET!,
      },
      params: {
        grant_type: 'client_credentials'
      }
    }
    try {
      const response = await axios.post(SpotifyTokenEndpoint, "", authOptions);
      this.access_token = response.data.access_token;
      this.token_expiration = Date.now() + response.data.expires_in * 1000;
    } catch (error) {
      console.error(error);
    }
  }

  async getPlaylist(playlistId: string): Promise<Spotify.PlaylistObject> {
    await this.requestToken();
    return this.get(`playlists/${playlistId}`);
  }

  async getPlaylistTracks(playlistUrl: string, forUrl: boolean): Promise<Spotify.PagingObject<Spotify.PlaylistTrackObject>> {
    await this.requestToken();
    return this.get(playlistUrl);
  }

  async getTrack(trackId: string): Promise<Spotify.TrackObject> {
    await this.requestToken();
    return this.get(`tracks/${trackId}`);
  }

  async getAudioFeatures(trackId: string): Promise<Spotify.AudioFeaturesObject> {
    await this.requestToken();
    return this.get(`audio-features/${trackId}`);
  }
}
