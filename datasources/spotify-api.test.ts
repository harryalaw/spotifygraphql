import { SpotifyApi } from "./spotify-api";
import axios from "axios";

jest.mock("axios");

describe("Spotify API", () => {
  afterEach(() => {
    jest.clearAllMocks();
  });
  const mockDate = new Date("2021-09-18T00:00:00.000Z").valueOf();

  jest.spyOn(global.Date, "now")
    .mockImplementation(() => mockDate)

  let spotifyApi = new SpotifyApi();

  const mockedAxios = axios as jest.Mocked<typeof axios>;

  const expires_in = 7;
  const access_token = "access_token";
  const axiosResponse = Promise.resolve({
    data: {
      access_token,
      expires_in
    }
  });
  mockedAxios.post.mockResolvedValue(axiosResponse);

  describe("requestToken()", () => {
    it("If expiration date is in the future and have access_token doesn't call axios", async () => {
      spotifyApi.token_expiration = Date.now() + 100000;
      spotifyApi.access_token = "access_token";
      await spotifyApi.requestToken();

      expect(mockedAxios.post).not.toHaveBeenCalled();
      expect(mockedAxios.options).not.toHaveBeenCalled();
    });

    it("if access_token is empty then calls axios", async () => {
      spotifyApi.access_token = "";
      spotifyApi.token_expiration = Date.now() + 100000;

      await spotifyApi.requestToken();
      expect(mockedAxios.post).toHaveBeenCalledTimes(1);

      expect(spotifyApi.access_token).toEqual(access_token);
      expect(spotifyApi.token_expiration).toEqual(Date.now() + expires_in * 1000);
    });

    it("if expiration date is in the past then calls axios", async () => {
      spotifyApi.access_token = "access_token";
      spotifyApi.token_expiration = Date.now() - 1000000;

      await spotifyApi.requestToken();
      expect(mockedAxios.post).toHaveBeenCalledTimes(1);

      expect(spotifyApi.access_token).toEqual(access_token);
      expect(spotifyApi.token_expiration).toEqual(Date.now() + expires_in * 1000);
    });

  });
});