export namespace Spotify {

  export type ArtistObject = {
    external_urls: ExternalUrlObject,
    followers: FollowersObject,
    genres: Array<string>
    href: string,
    id: string,
    images: Array<ImageObject>,
    name: string,
    popularity: number,
    type: string,
    uri: string
  }

  export type AudioFeaturesObject = {
    acousticness: number,
    analysis_url: string,
    danceability: number,
    duration_ms: number,
    energy: number,
    id: string,
    instrumentalness: number,
    key: 0 | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 | 10 | 11,
    liveness: number,
    loudness: number,
    mode: 0 | 1,
    speechiness: number,
    tempo: number,
    time_signature: number,
    track_href: "audio_features",
    uri: string,
    valence: number,
  }

  export type EpisodeObject = {
    audio_preview_url: string,
    description: string,
    duration_ms: number,
    explicit: boolean,
    external_urls: ExternalUrlObject,
    href: string,
    html_description: string,
    id: string,
    images: Array<ImageObject>,
    is_externally_hosted: boolean,
    is_playable: boolean,
    languages: Array<string>,
    name: string,
    release_date: string,
    release_date_precision: 'year' | 'month' | 'day',
    restrictions: Restrictions,
    resume_point: ResumePointObject,
    type: string,
    uri: string,
  }

  export type ExternalIdObject = {
    ean?: string,
    isrc?: string,
    upc?: string
  }

  export type ExternalUrlObject = {
    spotify: string,
  }

  export type FollowersObject = {
    href: string | null,
    total: number,
  }

  export type ImageObject = {
    height: number,
    url: string,
    width: number
  }

  export type PagingObject<T> = {
    href: string,
    items: Array<T>,
    limit: number,
    next: string | null,
    offset: number,
    previous: string | null,
    total: number
  }

  export type PlaylistObject = {
    collaborative: boolean,
    description: string | null,
    external_urls: ExternalUrlObject,
    followers: FollowersObject,
    href: string,
    id: string,
    images: Array<ImageObject>,
    name: string,
    owner: PublicUserObject,
    primary_color: string | null,
    public: boolean | null,
    snapshot_id: string,
    tracks: PagingObject<PlaylistTrackObject>,
    type: 'playlist',
    uri: string
  }

  export type PlaylistTrackObject = {
    added_at: string | null,
    added_by: PublicUserObject,
    is_local: boolean,
    primary_color?: string | null,
    track: TrackObject | EpisodeObject
    video_thumbnail: {
      url: string | null,
    }
  }

  export type PublicUserObject = {
    display_name?: string | null,
    external_urls: ExternalUrlObject,
    followers?: FollowersObject,
    href: string,
    id: string,
    images?: Array<ImageObject>,
    type: string,
    uri: string
  }

  export type Restrictions = {
    reason: 'market' | 'product' | 'explicit'
  }

  export type ResumePointObject = {
    fully_played: boolean,
    resume_position_ms: number
  }

  export type SimplifiedAlbumObject = {
    album_type: string,
    artists: Array<SimplifiedArtistObject>,
    available_markets: Array<string>,
    external_urls: ExternalUrlObject,
    href: string,
    id: string,
    images: Array<ImageObject>,
    name: string,
    release_date: string,
    release_date_precision: 'year' | 'month' | 'day',
    total_tracks: number,
    type: 'album',
    uri: string,
  }

  export type SimplifiedArtistObject = {
    external_urls: ExternalUrlObject,
    href: string,
    id: string,
    name: string,
    type: string,
    uri: string,
  }

  export type TrackObject = {
    album: SimplifiedAlbumObject,
    artists: Array<SimplifiedArtistObject>,
    available_markets: Array<string>,
    disc_number: number,
    duration_ms: number,
    episode: false,
    explicit: boolean,
    external_ids: ExternalIdObject,
    external_urls: ExternalUrlObject,
    href: string,
    id: string,
    is_local: boolean,
    is_playable?: boolean,
    // linked_from:
    name: string,
    popularity: number,
    preview_url: string,
    restrictions?: Restrictions,
    track: true,
    track_number: number,
    type: 'track',
    uri: string
  }

}