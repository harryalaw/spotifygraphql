// these interfaces might help us figure out what our schema might look like
import { gql } from 'apollo-server';


const typeDefs = gql`
  type Query {
    song(id: ID!): SpotifySong!
    playlist(id: ID!): SpotifyPlaylist!
    audioFeatures(id: ID!): AudioFeatures!
  }

  "A representative of an artist on spotify"
  type SpotifyArtist {
    "The id that spotify uses to identify an artist"
    id: ID!
    "The name of the spotify artist"
    name: String!,
  }

  "An image from spotify"
  type SpotifyImage {
    "The height of the image"
    height: String,
    "The width of the image"
    width: String,
    "The url that the image is hosted at"
    url: String!
  }

  "A representation of a song on spotify"
  type SpotifySong {
    "The id that uniquely identifies this song"
    id: ID!,
    "The name of the song"
    name: String!,
    "The album the song appears on"
    album: SpotifyAlbum!,
    "The artists that feature on the song"
    artists: [SpotifyArtist!]!,
    "The position in the album"
    track_number: Int
    "The length of the song in ms"
    duration_ms: Int,
    "An audio analysis of a spotify song"
    audio_features: AudioFeatures
  }

  "Audio analysis from Spotify"
  type AudioFeatures {
    """A measure from 0.0 to 1.0 of whether the track is acoustic 
       or not. 1.0 is high confidence the track is acoustic"""
    acousticness: Float,
    """A URL to access the full audio analysis of this track"""
    analysis_url: String,
    "0.0 is least danceable, 1.0 is most danceable"
    danceability: Float,
    "Length of the track in milliseconds"
    duration_ms: Int,
    "0.0 is low energy, 1.0 is high energy"
    energy: Float,
    "The Spotify ID for the track"
    id: String,
    "0.0 is not instrumental, 0.5 represents instrumentals but confidence better closer to 1.0"
    instrumentalness: Float,
    "The key the track is in, using Pitch Class Notation so 0 = C, 1 = C# ..."
    key: Int,
    "0.8 represents a strong propoability that the track was performed live"
    liveness: Float,
    "The overall loudness of a track in decibels. Typically between -60 and 0 dB"
    loudness: Float,
    "Whether the track is Major (1) or Minor (0)"
    mode: Int,
    "Detects spoken word, closer to 1.0 the more speechy it is. 0.33> is not speech, 0.33<0.66 may contain music and speech, 0.66< are speech"
    speechiness: Float,
    "The tempo of the track in BPM"
    tempo: Float,
    "An estimated time signature of the track"
    time_signature: Int,
    "A link to the web API endpoint for full details of the track"
    track_href: String,
    "The object type"
    type: String,
    "The spotify URI for the track"
    uri: String,
    "Closer to 1.0 is happy, 0.0 are more sad"
    valence: Float,
  }

  "A representation of an album on spotify"
  type SpotifyAlbum {
    "The id that uniquely identifies this album"
    id: ID!,
    "The name of the album"
    name: String!,
    "The artists that feature on the album"
    artists: [SpotifyArtist!]!,
    "The release date of the album"
    release_date: String!
    "The album artwork"
    images: [SpotifyImage!]!
  }

  "A representation of a playlist on Spotify"
  type SpotifyPlaylist {
    "The id that uniquel identifies this playlist"
    id: ID!,
    "The name of the playlist"
    name: String!,
    "The playlist images"
    images: [SpotifyImage!]!,
    "The tracks of a playlist"
    tracks: [SpotifySong!]!
    "The number of tracks a playlist has"
    track_count: Int
    "An id to indicate what instance of the playlist has been fetched"
    snapshot_id: String,
  }
`

export { typeDefs };

export interface SpotifyArtist {
  name: string,
  id: string
}

export interface SpotifyImage {
  height?: number,
  width?: number,
  url: string
}

export interface SpotifySong {
  album: SpotifyAlbum,
  artists: Array<SpotifyArtist>
  id: string,
  name: string,
  track_number: number
}

export interface SpotifyAlbum {
  artists: Array<SpotifyArtist>,
  name: string,
  release_date: string,
  id: string,
  images: Array<SpotifyImage>
}

export interface SpotifyPlaylist {
  images: Array<SpotifyImage>,
  name: string,
  tracks: {
    total: number,
  }
  id: string,
  snapshot_id: string,
}
