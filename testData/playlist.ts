import { Spotify } from "../@types/SpotifyObjects";

export const user: Spotify.PublicUserObject = {
  external_urls: {
    spotify: "https://open.spotify.com/user/crazyfishlord"
  },
  href: "https://api.spotify.com/v1/users/crazyfishlord",
  id: "crazyfishlord",
  type: "user",
  uri: "spotify:user:crazyfishlord"
};

export const playlistTracksResponse = (): Spotify.PagingObject<Spotify.PlaylistTrackObject> => {

  return {
    href: "https://api.spotify.com/v1/playlists/4Cca115KKLWcnRTKqRq5wj/tracks?offset=5&limit=5",
    items: [
      {
        added_at: "2021-08-09T07:46:43Z",
        added_by: {
          external_urls: {
            spotify: "https://open.spotify.com/user/crazyfishlord"
          },
          href: "https://api.spotify.com/v1/users/crazyfishlord",
          id: "crazyfishlord",
          type: "user",
          uri: "spotify:user:crazyfishlord"
        },
        is_local: false,
        primary_color: null,
        track: {
          album: {
            album_type: "compilation",
            artists: [
              {
                external_urls: {
                  spotify: "https://open.spotify.com/artist/0LyfQWJT6nXafLPZqxe9Of"
                },
                href: "https://api.spotify.com/v1/artists/0LyfQWJT6nXafLPZqxe9Of",
                id: "0LyfQWJT6nXafLPZqxe9Of",
                name: "Various Artists",
                type: "artist",
                uri: "spotify:artist:0LyfQWJT6nXafLPZqxe9Of"
              }
            ],
            available_markets: [
              'GB'
            ],
            external_urls: {
              spotify: "https://open.spotify.com/album/0dOhH2RgLPvr3698DcZVp1"
            },
            href: "https://api.spotify.com/v1/albums/0dOhH2RgLPvr3698DcZVp1",
            id: "0dOhH2RgLPvr3698DcZVp1",
            images: [
              {
                height: 640,
                url: "https://i.scdn.co/image/ab67616d0000b273b6d9c57925fdbf5eeef2d077",
                width: 640
              },
              {
                height: 300,
                url: "https://i.scdn.co/image/ab67616d00001e02b6d9c57925fdbf5eeef2d077",
                width: 300
              },
              {
                height: 64,
                url: "https://i.scdn.co/image/ab67616d00004851b6d9c57925fdbf5eeef2d077",
                width: 64
              }
            ],
            name: "Free Soul Nujabes First Collection",
            release_date: "2014-12-17",
            release_date_precision: "day",
            total_tracks: 17,
            type: "album",
            uri: "spotify:album:0dOhH2RgLPvr3698DcZVp1"
          },
          artists: [
            {
              external_urls: {
                spotify: "https://open.spotify.com/artist/3Rq3YOF9YG9YfCWD4D56RZ"
              },
              href: "https://api.spotify.com/v1/artists/3Rq3YOF9YG9YfCWD4D56RZ",
              id: "3Rq3YOF9YG9YfCWD4D56RZ",
              name: "Nujabes",
              type: "artist",
              uri: "spotify:artist:3Rq3YOF9YG9YfCWD4D56RZ"
            },
            {
              external_urls: {
                spotify: "https://open.spotify.com/artist/4wnn2vMPzvlhSDwdZWiRpE"
              },
              href: "https://api.spotify.com/v1/artists/4wnn2vMPzvlhSDwdZWiRpE",
              id: "4wnn2vMPzvlhSDwdZWiRpE",
              name: "Cise Starr from CYNE",
              type: "artist",
              uri: "spotify:artist:4wnn2vMPzvlhSDwdZWiRpE"
            },
            {
              external_urls: {
                spotify: "https://open.spotify.com/artist/3e4UwgMHCFrHfscoDigdaT"
              },
              href: "https://api.spotify.com/v1/artists/3e4UwgMHCFrHfscoDigdaT",
              id: "3e4UwgMHCFrHfscoDigdaT",
              name: "Akin from CYNE",
              type: "artist",
              uri: "spotify:artist:3e4UwgMHCFrHfscoDigdaT"
            }
          ],
          available_markets: [
            'GB'
          ],
          disc_number: 1,
          duration_ms: 172266,
          episode: false,
          explicit: false,
          external_ids: {
            isrc: "JPH491605000"
          },
          external_urls: {
            spotify: "https://open.spotify.com/track/5f4GxONfMbpqRBVVt9ekh9"
          },
          href: "https://api.spotify.com/v1/tracks/5f4GxONfMbpqRBVVt9ekh9",
          id: "5f4GxONfMbpqRBVVt9ekh9",
          is_local: false,
          name: "Feather",
          popularity: 38,
          preview_url: "https://p.scdn.co/mp3-preview/fa3b5bf736f5cdbfaae5ed9e1f9f8ed899a5294f?cid=774b29d4f13844c495f206cafdad9c86",
          track: true,
          track_number: 3,
          type: "track",
          uri: "spotify:track:5f4GxONfMbpqRBVVt9ekh9"
        },
        video_thumbnail: {
          url: null
        }
      },
      {
        added_at: "2021-08-09T07:46:52Z",
        added_by: {
          external_urls: {
            spotify: "https://open.spotify.com/user/crazyfishlord"
          },
          href: "https://api.spotify.com/v1/users/crazyfishlord",
          id: "crazyfishlord",
          type: "user",
          uri: "spotify:user:crazyfishlord"
        },
        is_local: false,
        primary_color: null,
        track: {
          album: {
            album_type: "album",
            artists: [
              {
                external_urls: {
                  spotify: "https://open.spotify.com/artist/4oDjh8wNW5vDHyFRrDYC4k"
                },
                href: "https://api.spotify.com/v1/artists/4oDjh8wNW5vDHyFRrDYC4k",
                id: "4oDjh8wNW5vDHyFRrDYC4k",
                name: "Loyle Carner",
                type: "artist",
                uri: "spotify:artist:4oDjh8wNW5vDHyFRrDYC4k"
              }
            ],
            available_markets: [
              'GB'
            ],
            external_urls: {
              spotify: "https://open.spotify.com/album/6wjryxtrKxzTZID9kyZUV5"
            },
            href: "https://api.spotify.com/v1/albums/6wjryxtrKxzTZID9kyZUV5",
            id: "6wjryxtrKxzTZID9kyZUV5",
            images: [
              {
                height: 640,
                url: "https://i.scdn.co/image/ab67616d0000b27398ccb0cb6323cab21090467b",
                width: 640
              },
              {
                height: 300,
                url: "https://i.scdn.co/image/ab67616d00001e0298ccb0cb6323cab21090467b",
                width: 300
              },
              {
                height: 64,
                url: "https://i.scdn.co/image/ab67616d0000485198ccb0cb6323cab21090467b",
                width: 64
              }
            ],
            name: "Yesterday's Gone",
            release_date: "2017-01-20",
            release_date_precision: "day",
            total_tracks: 15,
            type: "album",
            uri: "spotify:album:6wjryxtrKxzTZID9kyZUV5"
          },
          artists: [
            {
              external_urls: {
                spotify: "https://open.spotify.com/artist/4oDjh8wNW5vDHyFRrDYC4k"
              },
              href: "https://api.spotify.com/v1/artists/4oDjh8wNW5vDHyFRrDYC4k",
              id: "4oDjh8wNW5vDHyFRrDYC4k",
              name: "Loyle Carner",
              type: "artist",
              uri: "spotify:artist:4oDjh8wNW5vDHyFRrDYC4k"
            },
            {
              external_urls: {
                spotify: "https://open.spotify.com/artist/1uiEZYehlNivdK3iQyAbye"
              },
              href: "https://api.spotify.com/v1/artists/1uiEZYehlNivdK3iQyAbye",
              id: "1uiEZYehlNivdK3iQyAbye",
              name: "Tom Misch",
              type: "artist",
              uri: "spotify:artist:1uiEZYehlNivdK3iQyAbye"
            }
          ],
          available_markets: [
            'GB'
          ],
          disc_number: 1,
          duration_ms: 172653,
          episode: false,
          explicit: true,
          external_ids: {
            isrc: "GBUM71605992"
          },
          external_urls: {
            spotify: "https://open.spotify.com/track/32sBVB5HmrwJ6e9g0S2BRL"
          },
          href: "https://api.spotify.com/v1/tracks/32sBVB5HmrwJ6e9g0S2BRL",
          id: "32sBVB5HmrwJ6e9g0S2BRL",
          is_local: false,
          name: "Damselfly",
          popularity: 64,
          preview_url: "https://p.scdn.co/mp3-preview/58d01f114b439e7452e650cc3a25fbbed9803365?cid=774b29d4f13844c495f206cafdad9c86",
          track: true,
          track_number: 4,
          type: "track",
          uri: "spotify:track:32sBVB5HmrwJ6e9g0S2BRL"
        },
        video_thumbnail: {
          url: null
        }
      },
      {
        added_at: "2021-08-09T07:47:01Z",
        added_by: {
          external_urls: {
            spotify: "https://open.spotify.com/user/crazyfishlord"
          },
          href: "https://api.spotify.com/v1/users/crazyfishlord",
          id: "crazyfishlord",
          type: "user",
          uri: "spotify:user:crazyfishlord"
        },
        is_local: false,
        primary_color: null,
        track: {
          album: {
            album_type: "single",
            artists: [
              {
                external_urls: {
                  spotify: "https://open.spotify.com/artist/5RTLRtXjbXI2lSXc6jxlAz"
                },
                href: "https://api.spotify.com/v1/artists/5RTLRtXjbXI2lSXc6jxlAz",
                id: "5RTLRtXjbXI2lSXc6jxlAz",
                name: "Ravyn Lenae",
                type: "artist",
                uri: "spotify:artist:5RTLRtXjbXI2lSXc6jxlAz"
              }
            ],
            available_markets: [
              'GB'
            ],
            external_urls: {
              spotify: "https://open.spotify.com/album/57X0V74PxWKM2fuyf283tE"
            },
            href: "https://api.spotify.com/v1/albums/57X0V74PxWKM2fuyf283tE",
            id: "57X0V74PxWKM2fuyf283tE",
            images: [
              {
                height: 640,
                url: "https://i.scdn.co/image/ab67616d0000b273087048fafaf1faae347c660a",
                width: 640
              },
              {
                height: 300,
                url: "https://i.scdn.co/image/ab67616d00001e02087048fafaf1faae347c660a",
                width: 300
              },
              {
                height: 64,
                url: "https://i.scdn.co/image/ab67616d00004851087048fafaf1faae347c660a",
                width: 64
              }
            ],
            name: "Moon Shoes EP",
            release_date: "2016-07-29",
            release_date_precision: "day",
            total_tracks: 10,
            type: "album",
            uri: "spotify:album:57X0V74PxWKM2fuyf283tE"
          },
          artists: [
            {
              external_urls: {
                spotify: "https://open.spotify.com/artist/5RTLRtXjbXI2lSXc6jxlAz"
              },
              href: "https://api.spotify.com/v1/artists/5RTLRtXjbXI2lSXc6jxlAz",
              id: "5RTLRtXjbXI2lSXc6jxlAz",
              name: "Ravyn Lenae",
              type: "artist",
              uri: "spotify:artist:5RTLRtXjbXI2lSXc6jxlAz"
            },
            {
              external_urls: {
                spotify: "https://open.spotify.com/artist/4Y2i9jhU3jW0PVsvTLIbWX"
              },
              href: "https://api.spotify.com/v1/artists/4Y2i9jhU3jW0PVsvTLIbWX",
              id: "4Y2i9jhU3jW0PVsvTLIbWX",
              name: "Appleby",
              type: "artist",
              uri: "spotify:artist:4Y2i9jhU3jW0PVsvTLIbWX"
            }
          ],
          available_markets: [
            'GB'
          ],
          disc_number: 1,
          duration_ms: 217013,
          episode: false,
          explicit: false,
          external_ids: {
            isrc: "USAT21602241"
          },
          external_urls: {
            spotify: "https://open.spotify.com/track/3HEn14GqygLCNfroOnYiZb"
          },
          href: "https://api.spotify.com/v1/tracks/3HEn14GqygLCNfroOnYiZb",
          id: "3HEn14GqygLCNfroOnYiZb",
          is_local: false,
          name: "Free Room (feat. Appleby)",
          popularity: 57,
          preview_url: "https://p.scdn.co/mp3-preview/e9de1df33c131b03920f34c1ca2acaa1cd2e727e?cid=774b29d4f13844c495f206cafdad9c86",
          track: true,
          track_number: 5,
          type: "track",
          uri: "spotify:track:3HEn14GqygLCNfroOnYiZb"
        },
        video_thumbnail: {
          url: null
        }
      },
      {
        added_at: "2021-08-09T07:47:13Z",
        added_by: {
          external_urls: {
            spotify: "https://open.spotify.com/user/crazyfishlord"
          },
          href: "https://api.spotify.com/v1/users/crazyfishlord",
          id: "crazyfishlord",
          type: "user",
          uri: "spotify:user:crazyfishlord"
        },
        is_local: false,
        primary_color: null,
        track: {
          album: {
            album_type: "single",
            artists: [
              {
                external_urls: {
                  spotify: "https://open.spotify.com/artist/5iRM7qYip6UNfQaPe2reCz"
                },
                href: "https://api.spotify.com/v1/artists/5iRM7qYip6UNfQaPe2reCz",
                id: "5iRM7qYip6UNfQaPe2reCz",
                name: "Barney Artist",
                type: "artist",
                uri: "spotify:artist:5iRM7qYip6UNfQaPe2reCz"
              }
            ],
            available_markets: [
              'GB'
            ],
            external_urls: {
              spotify: "https://open.spotify.com/album/5tbvov0fii1eMXkTYZmVdp"
            },
            href: "https://api.spotify.com/v1/albums/5tbvov0fii1eMXkTYZmVdp",
            id: "5tbvov0fii1eMXkTYZmVdp",
            images: [
              {
                height: 640,
                url: "https://i.scdn.co/image/ab67616d0000b27317db8995ea927774e506ad08",
                width: 640
              },
              {
                height: 300,
                url: "https://i.scdn.co/image/ab67616d00001e0217db8995ea927774e506ad08",
                width: 300
              },
              {
                height: 64,
                url: "https://i.scdn.co/image/ab67616d0000485117db8995ea927774e506ad08",
                width: 64
              }
            ],
            name: "Good to Be Home (feat. Tom Misch, Loyle Carner & Rebel Kleff)",
            release_date: "2018-10-03",
            release_date_precision: "day",
            total_tracks: 1,
            type: "album",
            uri: "spotify:album:5tbvov0fii1eMXkTYZmVdp"
          },
          artists: [
            {
              external_urls: {
                spotify: "https://open.spotify.com/artist/5iRM7qYip6UNfQaPe2reCz"
              },
              href: "https://api.spotify.com/v1/artists/5iRM7qYip6UNfQaPe2reCz",
              id: "5iRM7qYip6UNfQaPe2reCz",
              name: "Barney Artist",
              type: "artist",
              uri: "spotify:artist:5iRM7qYip6UNfQaPe2reCz"
            },
            {
              external_urls: {
                spotify: "https://open.spotify.com/artist/1uiEZYehlNivdK3iQyAbye"
              },
              href: "https://api.spotify.com/v1/artists/1uiEZYehlNivdK3iQyAbye",
              id: "1uiEZYehlNivdK3iQyAbye",
              name: "Tom Misch",
              type: "artist",
              uri: "spotify:artist:1uiEZYehlNivdK3iQyAbye"
            },
            {
              external_urls: {
                spotify: "https://open.spotify.com/artist/4oDjh8wNW5vDHyFRrDYC4k"
              },
              href: "https://api.spotify.com/v1/artists/4oDjh8wNW5vDHyFRrDYC4k",
              id: "4oDjh8wNW5vDHyFRrDYC4k",
              name: "Loyle Carner",
              type: "artist",
              uri: "spotify:artist:4oDjh8wNW5vDHyFRrDYC4k"
            },
            {
              external_urls: {
                spotify: "https://open.spotify.com/artist/4IZnj59pur7nfqpdGzuPF6"
              },
              href: "https://api.spotify.com/v1/artists/4IZnj59pur7nfqpdGzuPF6",
              id: "4IZnj59pur7nfqpdGzuPF6",
              name: "Rebel Kleff",
              type: "artist",
              uri: "spotify:artist:4IZnj59pur7nfqpdGzuPF6"
            }
          ],
          available_markets: [
            'GB'
          ],
          disc_number: 1,
          duration_ms: 263089,
          episode: false,
          explicit: false,
          external_ids: {
            isrc: "GBKPL1825438"
          },
          external_urls: {
            spotify: "https://open.spotify.com/track/2Sh3g3NaqkD23mZ6Ugfrsq"
          },
          href: "https://api.spotify.com/v1/tracks/2Sh3g3NaqkD23mZ6Ugfrsq",
          id: "2Sh3g3NaqkD23mZ6Ugfrsq",
          is_local: false,
          name: "Good to Be Home (feat. Tom Misch, Loyle Carner & Rebel Kleff)",
          popularity: 51,
          preview_url: "https://p.scdn.co/mp3-preview/38d58d81b1d25394766dc87d3448d19dc1865f57?cid=774b29d4f13844c495f206cafdad9c86",
          track: true,
          track_number: 1,
          type: "track",
          uri: "spotify:track:2Sh3g3NaqkD23mZ6Ugfrsq"
        },
        video_thumbnail: {
          url: null
        }
      },
      {
        added_at: "2021-08-09T07:47:22Z",
        added_by: {
          external_urls: {
            spotify: "https://open.spotify.com/user/crazyfishlord"
          },
          href: "https://api.spotify.com/v1/users/crazyfishlord",
          id: "crazyfishlord",
          type: "user",
          uri: "spotify:user:crazyfishlord"
        },
        is_local: false,
        primary_color: null,
        track: {
          album: {
            album_type: "album",
            artists: [
              {
                external_urls: {
                  spotify: "https://open.spotify.com/artist/3JWTfcIZq4OUdC6oBunofK"
                },
                href: "https://api.spotify.com/v1/artists/3JWTfcIZq4OUdC6oBunofK",
                id: "3JWTfcIZq4OUdC6oBunofK",
                name: "Otis Junior",
                type: "artist",
                uri: "spotify:artist:3JWTfcIZq4OUdC6oBunofK"
              },
              {
                external_urls: {
                  spotify: "https://open.spotify.com/artist/6T2NShr7SAArhtegdIpHHN"
                },
                href: "https://api.spotify.com/v1/artists/6T2NShr7SAArhtegdIpHHN",
                id: "6T2NShr7SAArhtegdIpHHN",
                name: "Dr. Dundiff",
                type: "artist",
                uri: "spotify:artist:6T2NShr7SAArhtegdIpHHN"
              }
            ],
            available_markets: [
              'GB'
            ],
            external_urls: {
              spotify: "https://open.spotify.com/album/7iLIbwBXBk93mFQDQu8e36"
            },
            href: "https://api.spotify.com/v1/albums/7iLIbwBXBk93mFQDQu8e36",
            id: "7iLIbwBXBk93mFQDQu8e36",
            images: [
              {
                height: 640,
                url: "https://i.scdn.co/image/ab67616d0000b27314851c213e301528a655d65c",
                width: 640
              },
              {
                height: 300,
                url: "https://i.scdn.co/image/ab67616d00001e0214851c213e301528a655d65c",
                width: 300
              },
              {
                height: 64,
                url: "https://i.scdn.co/image/ab67616d0000485114851c213e301528a655d65c",
                width: 64
              }
            ],
            name: "1Moment2Another",
            release_date: "2016-04-22",
            release_date_precision: "day",
            total_tracks: 8,
            type: "album",
            uri: "spotify:album:7iLIbwBXBk93mFQDQu8e36"
          },
          artists: [
            {
              external_urls: {
                spotify: "https://open.spotify.com/artist/3JWTfcIZq4OUdC6oBunofK"
              },
              href: "https://api.spotify.com/v1/artists/3JWTfcIZq4OUdC6oBunofK",
              id: "3JWTfcIZq4OUdC6oBunofK",
              name: "Otis Junior",
              type: "artist",
              uri: "spotify:artist:3JWTfcIZq4OUdC6oBunofK"
            },
            {
              external_urls: {
                spotify: "https://open.spotify.com/artist/6T2NShr7SAArhtegdIpHHN"
              },
              href: "https://api.spotify.com/v1/artists/6T2NShr7SAArhtegdIpHHN",
              id: "6T2NShr7SAArhtegdIpHHN",
              name: "Dr. Dundiff",
              type: "artist",
              uri: "spotify:artist:6T2NShr7SAArhtegdIpHHN"
            }
          ],
          available_markets: [
            'GB'
          ],
          disc_number: 1,
          duration_ms: 247619,
          episode: false,
          explicit: false,
          external_ids: {
            isrc: "DESY21400359"
          },
          external_urls: {
            spotify: "https://open.spotify.com/track/1RaJMyCatBXw5hGCpVzTp4"
          },
          href: "https://api.spotify.com/v1/tracks/1RaJMyCatBXw5hGCpVzTp4",
          id: "1RaJMyCatBXw5hGCpVzTp4",
          is_local: false,
          name: "Me vs. Me",
          popularity: 38,
          preview_url: "https://p.scdn.co/mp3-preview/142062f478c1d9a914f7553c522c09a6ccc687e9?cid=774b29d4f13844c495f206cafdad9c86",
          track: true,
          track_number: 2,
          type: "track",
          uri: "spotify:track:1RaJMyCatBXw5hGCpVzTp4"
        },
        video_thumbnail: {
          url: null
        }
      }
    ],
    limit: 5,
    next: null,
    offset: 5,
    previous: "https://api.spotify.com/v1/playlists/4Cca115KKLWcnRTKqRq5wj/tracks?offset=0&limit=5",
    total: 10
  }
}

export const playlistResponse = (): Spotify.PlaylistObject => {
  return {
    collaborative: false,
    description: "",
    external_urls: {
      spotify: "https://open.spotify.com/playlist/4Cca115KKLWcnRTKqRq5wj"
    },
    followers: {
      href: null,
      total: 0
    },
    href: "https://api.spotify.com/v1/playlists/4Cca115KKLWcnRTKqRq5wj",
    id: "4Cca115KKLWcnRTKqRq5wj",
    images: [
      {
        height: 640,
        url: "https://mosaic.scdn.co/640/ab67616d0000b27324e3b5e9febd5f747c681c3dab67616d0000b273482be3c0abad6a3c2bcd2efdab67616d0000b27384501102111aef7bc85a28c5ab67616d0000b2739e22cb54031d665358c919a6",
        width: 640
      },
      {
        height: 300,
        url: "https://mosaic.scdn.co/300/ab67616d0000b27324e3b5e9febd5f747c681c3dab67616d0000b273482be3c0abad6a3c2bcd2efdab67616d0000b27384501102111aef7bc85a28c5ab67616d0000b2739e22cb54031d665358c919a6",
        width: 300
      },
      {
        height: 60,
        url: "https://mosaic.scdn.co/60/ab67616d0000b27324e3b5e9febd5f747c681c3dab67616d0000b273482be3c0abad6a3c2bcd2efdab67616d0000b27384501102111aef7bc85a28c5ab67616d0000b2739e22cb54031d665358c919a6",
        width: 60
      }
    ],
    name: "Bluebird",
    owner: {
      display_name: "harryalaw",
      external_urls: {
        spotify: "https://open.spotify.com/user/crazyfishlord"
      },
      href: "https://api.spotify.com/v1/users/crazyfishlord",
      id: "crazyfishlord",
      type: "user",
      uri: "spotify:user:crazyfishlord"
    },
    primary_color: null,
    public: false,
    snapshot_id: "MTEsMWEzMmU3Y2M3NzZmZDBhYjUxNjA0MzIxMjYxMjhmZmYyYTU0M2QyYg==",
    tracks: {
      href: "https://api.spotify.com/v1/playlists/4Cca115KKLWcnRTKqRq5wj/tracks?offset=0&limit=5",
      items: [
        {
          added_at: "2021-08-09T07:45:40Z",
          added_by: {
            external_urls: {
              spotify: "https://open.spotify.com/user/crazyfishlord"
            },
            href: "https://api.spotify.com/v1/users/crazyfishlord",
            id: "crazyfishlord",
            type: "user",
            uri: "spotify:user:crazyfishlord"
          },
          is_local: false,
          primary_color: null,
          track: {
            album: {
              album_type: "album",
              artists: [
                {
                  external_urls: {
                    spotify: "https://open.spotify.com/artist/6r7dk6cptOXRAk3Bm7iouK"
                  },
                  href: "https://api.spotify.com/v1/artists/6r7dk6cptOXRAk3Bm7iouK",
                  id: "6r7dk6cptOXRAk3Bm7iouK",
                  name: "One Self",
                  type: "artist",
                  uri: "spotify:artist:6r7dk6cptOXRAk3Bm7iouK"
                }
              ],
              available_markets: [
                'GB'
              ],
              external_urls: {
                spotify: "https://open.spotify.com/album/2GJdJQNe5lCntmUIArrE59"
              },
              href: "https://api.spotify.com/v1/albums/2GJdJQNe5lCntmUIArrE59",
              id: "2GJdJQNe5lCntmUIArrE59",
              images: [
                {
                  height: 640,
                  url: "https://i.scdn.co/image/ab67616d0000b27324e3b5e9febd5f747c681c3d",
                  width: 640
                },
                {
                  height: 300,
                  url: "https://i.scdn.co/image/ab67616d00001e0224e3b5e9febd5f747c681c3d",
                  width: 300
                },
                {
                  height: 64,
                  url: "https://i.scdn.co/image/ab67616d0000485124e3b5e9febd5f747c681c3d",
                  width: 64
                }
              ],
              name: "Children of Possibility",
              release_date: "2005",
              release_date_precision: "year",
              total_tracks: 12,
              type: "album",
              uri: "spotify:album:2GJdJQNe5lCntmUIArrE59"
            },
            artists: [
              {
                external_urls: {
                  spotify: "https://open.spotify.com/artist/6r7dk6cptOXRAk3Bm7iouK"
                },
                href: "https://api.spotify.com/v1/artists/6r7dk6cptOXRAk3Bm7iouK",
                id: "6r7dk6cptOXRAk3Bm7iouK",
                name: "One Self",
                type: "artist",
                uri: "spotify:artist:6r7dk6cptOXRAk3Bm7iouK"
              }
            ],
            available_markets: [
              'GB'
            ],
            disc_number: 1,
            duration_ms: 220413,
            episode: false,
            explicit: false,
            external_ids: {
              isrc: "GBCFB0501105"
            },
            external_urls: {
              spotify: "https://open.spotify.com/track/1MvH3FdvO17S2eQ4IeNRCo"
            },
            href: "https://api.spotify.com/v1/tracks/1MvH3FdvO17S2eQ4IeNRCo",
            id: "1MvH3FdvO17S2eQ4IeNRCo",
            is_local: false,
            name: "Bluebird",
            popularity: 49,
            preview_url: "https://p.scdn.co/mp3-preview/dfa9742559ad9c707dc8201f0be92f064c00d475?cid=774b29d4f13844c495f206cafdad9c86",
            track: true,
            track_number: 6,
            type: "track",
            uri: "spotify:track:1MvH3FdvO17S2eQ4IeNRCo"
          },
          video_thumbnail: {
            url: null
          }
        },
        {
          added_at: "2021-08-09T07:45:54Z",
          added_by: {
            external_urls: {
              spotify: "https://open.spotify.com/user/crazyfishlord"
            },
            href: "https://api.spotify.com/v1/users/crazyfishlord",
            id: "crazyfishlord",
            type: "user",
            uri: "spotify:user:crazyfishlord"
          },
          is_local: false,
          primary_color: null,
          track: {
            album: {
              album_type: "single",
              artists: [
                {
                  external_urls: {
                    spotify: "https://open.spotify.com/artist/1Q2mS59tFYLm2KGFoCgWN4"
                  },
                  href: "https://api.spotify.com/v1/artists/1Q2mS59tFYLm2KGFoCgWN4",
                  id: "1Q2mS59tFYLm2KGFoCgWN4",
                  name: "Samm Henshaw",
                  type: "artist",
                  uri: "spotify:artist:1Q2mS59tFYLm2KGFoCgWN4"
                }
              ],
              available_markets: [
                'GB'
              ],
              external_urls: {
                spotify: "https://open.spotify.com/album/7EFpevvrKa2TjXTFSaVR1z"
              },
              href: "https://api.spotify.com/v1/albums/7EFpevvrKa2TjXTFSaVR1z",
              id: "7EFpevvrKa2TjXTFSaVR1z",
              images: [
                {
                  height: 640,
                  url: "https://i.scdn.co/image/ab67616d0000b27384501102111aef7bc85a28c5",
                  width: 640
                },
                {
                  height: 300,
                  url: "https://i.scdn.co/image/ab67616d00001e0284501102111aef7bc85a28c5",
                  width: 300
                },
                {
                  height: 64,
                  url: "https://i.scdn.co/image/ab67616d0000485184501102111aef7bc85a28c5",
                  width: 64
                }
              ],
              name: "Broke",
              release_date: "2018-08-08",
              release_date_precision: "day",
              total_tracks: 2,
              type: "album",
              uri: "spotify:album:7EFpevvrKa2TjXTFSaVR1z"
            },
            artists: [
              {
                external_urls: {
                  spotify: "https://open.spotify.com/artist/1Q2mS59tFYLm2KGFoCgWN4"
                },
                href: "https://api.spotify.com/v1/artists/1Q2mS59tFYLm2KGFoCgWN4",
                id: "1Q2mS59tFYLm2KGFoCgWN4",
                name: "Samm Henshaw",
                type: "artist",
                uri: "spotify:artist:1Q2mS59tFYLm2KGFoCgWN4"
              }
            ],
            available_markets: [
              'GB'
            ],
            disc_number: 1,
            duration_ms: 207722,
            episode: false,
            explicit: false,
            external_ids: {
              isrc: "GBARL1800632"
            },
            external_urls: {
              spotify: "https://open.spotify.com/track/1JqcZQCnxjDOfZZwQidb1H"
            },
            href: "https://api.spotify.com/v1/tracks/1JqcZQCnxjDOfZZwQidb1H",
            id: "1JqcZQCnxjDOfZZwQidb1H",
            is_local: false,
            name: "Broke",
            popularity: 61,
            preview_url: "https://p.scdn.co/mp3-preview/c1fc3636af8b31184b235c46b4a079a36c332c7b?cid=774b29d4f13844c495f206cafdad9c86",
            track: true,
            track_number: 1,
            type: "track",
            uri: "spotify:track:1JqcZQCnxjDOfZZwQidb1H"
          },
          video_thumbnail: {
            url: null
          }
        },
        {
          added_at: "2021-08-09T07:46:04Z",
          added_by: {
            external_urls: {
              spotify: "https://open.spotify.com/user/crazyfishlord"
            },
            href: "https://api.spotify.com/v1/users/crazyfishlord",
            id: "crazyfishlord",
            type: "user",
            uri: "spotify:user:crazyfishlord"
          },
          is_local: false,
          primary_color: null,
          track: {
            album: {
              album_type: "album",
              artists: [
                {
                  external_urls: {
                    spotify: "https://open.spotify.com/artist/1O3ZOjqFLEnbpZexcRjocn"
                  },
                  href: "https://api.spotify.com/v1/artists/1O3ZOjqFLEnbpZexcRjocn",
                  id: "1O3ZOjqFLEnbpZexcRjocn",
                  name: "RJD2",
                  type: "artist",
                  uri: "spotify:artist:1O3ZOjqFLEnbpZexcRjocn"
                }
              ],
              available_markets: [
                'GB'
              ],
              external_urls: {
                spotify: "https://open.spotify.com/album/7DmNwRBDJRUEFUlk3oa2Aj"
              },
              href: "https://api.spotify.com/v1/albums/7DmNwRBDJRUEFUlk3oa2Aj",
              id: "7DmNwRBDJRUEFUlk3oa2Aj",
              images: [
                {
                  height: 640,
                  url: "https://i.scdn.co/image/ab67616d0000b273482be3c0abad6a3c2bcd2efd",
                  width: 640
                },
                {
                  height: 300,
                  url: "https://i.scdn.co/image/ab67616d00001e02482be3c0abad6a3c2bcd2efd",
                  width: 300
                },
                {
                  height: 64,
                  url: "https://i.scdn.co/image/ab67616d00004851482be3c0abad6a3c2bcd2efd",
                  width: 64
                }
              ],
              name: "Deadringer: Deluxe",
              release_date: "2002",
              release_date_precision: "year",
              total_tracks: 18,
              type: "album",
              uri: "spotify:album:7DmNwRBDJRUEFUlk3oa2Aj"
            },
            artists: [
              {
                external_urls: {
                  spotify: "https://open.spotify.com/artist/1O3ZOjqFLEnbpZexcRjocn"
                },
                href: "https://api.spotify.com/v1/artists/1O3ZOjqFLEnbpZexcRjocn",
                id: "1O3ZOjqFLEnbpZexcRjocn",
                name: "RJD2",
                type: "artist",
                uri: "spotify:artist:1O3ZOjqFLEnbpZexcRjocn"
              }
            ],
            available_markets: [
              'GB'
            ],
            disc_number: 1,
            duration_ms: 317933,
            episode: false,
            explicit: false,
            external_ids: {
              isrc: "USA4T0403506"
            },
            external_urls: {
              spotify: "https://open.spotify.com/track/5Nn2Dj7OQsGL6pgQ9iIzPp"
            },
            href: "https://api.spotify.com/v1/tracks/5Nn2Dj7OQsGL6pgQ9iIzPp",
            id: "5Nn2Dj7OQsGL6pgQ9iIzPp",
            is_local: false,
            name: "Ghostwriter",
            popularity: 60,
            preview_url: "https://p.scdn.co/mp3-preview/d079b16eb0cae5c54fe3f76df0219c72a2f1e173?cid=774b29d4f13844c495f206cafdad9c86",
            track: true,
            track_number: 6,
            type: "track",
            uri: "spotify:track:5Nn2Dj7OQsGL6pgQ9iIzPp"
          },
          video_thumbnail: {
            url: null
          }
        },
        {
          added_at: "2021-08-09T07:46:17Z",
          added_by: {
            external_urls: {
              spotify: "https://open.spotify.com/user/crazyfishlord"
            },
            href: "https://api.spotify.com/v1/users/crazyfishlord",
            id: "crazyfishlord",
            type: "user",
            uri: "spotify:user:crazyfishlord"
          },
          is_local: false,
          primary_color: null,
          track: {
            album: {
              album_type: "album",
              artists: [
                {
                  external_urls: {
                    spotify: "https://open.spotify.com/artist/7cIx4IDh0zLqXBxuNSqFNq"
                  },
                  href: "https://api.spotify.com/v1/artists/7cIx4IDh0zLqXBxuNSqFNq",
                  id: "7cIx4IDh0zLqXBxuNSqFNq",
                  name: "Rae Khalil",
                  type: "artist",
                  uri: "spotify:artist:7cIx4IDh0zLqXBxuNSqFNq"
                }
              ],
              available_markets: [
                'GB'
              ],
              external_urls: {
                spotify: "https://open.spotify.com/album/4qjamshmT7gedEq6uGHXLr"
              },
              href: "https://api.spotify.com/v1/albums/4qjamshmT7gedEq6uGHXLr",
              id: "4qjamshmT7gedEq6uGHXLr",
              images: [
                {
                  height: 640,
                  url: "https://i.scdn.co/image/ab67616d0000b2739e22cb54031d665358c919a6",
                  width: 640
                },
                {
                  height: 300,
                  url: "https://i.scdn.co/image/ab67616d00001e029e22cb54031d665358c919a6",
                  width: 300
                },
                {
                  height: 64,
                  url: "https://i.scdn.co/image/ab67616d000048519e22cb54031d665358c919a6",
                  width: 64
                }
              ],
              name: "FORTHEWORLD",
              release_date: "2020-06-09",
              release_date_precision: "day",
              total_tracks: 15,
              type: "album",
              uri: "spotify:album:4qjamshmT7gedEq6uGHXLr"
            },
            artists: [
              {
                external_urls: {
                  spotify: "https://open.spotify.com/artist/7cIx4IDh0zLqXBxuNSqFNq"
                },
                href: "https://api.spotify.com/v1/artists/7cIx4IDh0zLqXBxuNSqFNq",
                id: "7cIx4IDh0zLqXBxuNSqFNq",
                name: "Rae Khalil",
                type: "artist",
                uri: "spotify:artist:7cIx4IDh0zLqXBxuNSqFNq"
              }
            ],
            available_markets: [
              'GB'
            ],
            disc_number: 1,
            duration_ms: 145620,
            episode: false,
            explicit: false,
            external_ids: {
              isrc: "QZL382047176"
            },
            external_urls: {
              spotify: "https://open.spotify.com/track/6hDJWLmPebVbFrDC9hw1Ry"
            },
            href: "https://api.spotify.com/v1/tracks/6hDJWLmPebVbFrDC9hw1Ry",
            id: "6hDJWLmPebVbFrDC9hw1Ry",
            is_local: false,
            name: "benny!",
            popularity: 36,
            preview_url: "https://p.scdn.co/mp3-preview/62b5e6c16d9442a15eb55f31177076d32eae69aa?cid=774b29d4f13844c495f206cafdad9c86",
            track: true,
            track_number: 5,
            type: "track",
            uri: "spotify:track:6hDJWLmPebVbFrDC9hw1Ry"
          },
          video_thumbnail: {
            url: null
          }
        },
        {
          added_at: "2021-08-09T07:46:26Z",
          added_by: {
            external_urls: {
              spotify: "https://open.spotify.com/user/crazyfishlord"
            },
            href: "https://api.spotify.com/v1/users/crazyfishlord",
            id: "crazyfishlord",
            type: "user",
            uri: "spotify:user:crazyfishlord"
          },
          is_local: false,
          primary_color: null,
          track: {
            album: {
              album_type: "album",
              artists: [
                {
                  external_urls: {
                    spotify: "https://open.spotify.com/artist/1EpyA68dKpjf7jXmQL88Hy"
                  },
                  href: "https://api.spotify.com/v1/artists/1EpyA68dKpjf7jXmQL88Hy",
                  id: "1EpyA68dKpjf7jXmQL88Hy",
                  name: "Noname",
                  type: "artist",
                  uri: "spotify:artist:1EpyA68dKpjf7jXmQL88Hy"
                }
              ],
              available_markets: [
                'GB'
              ],
              external_urls: {
                spotify: "https://open.spotify.com/album/18Scpsg5OV1iYNtSaCsjwz"
              },
              href: "https://api.spotify.com/v1/albums/18Scpsg5OV1iYNtSaCsjwz",
              id: "18Scpsg5OV1iYNtSaCsjwz",
              images: [
                {
                  height: 640,
                  url: "https://i.scdn.co/image/ab67616d0000b273e9ef4d0bab9c9bf9ef5d3f36",
                  width: 640
                },
                {
                  height: 300,
                  url: "https://i.scdn.co/image/ab67616d00001e02e9ef4d0bab9c9bf9ef5d3f36",
                  width: 300
                },
                {
                  height: 64,
                  url: "https://i.scdn.co/image/ab67616d00004851e9ef4d0bab9c9bf9ef5d3f36",
                  width: 64
                }
              ],
              name: "Telefone",
              release_date: "2016-08-12",
              release_date_precision: "day",
              total_tracks: 10,
              type: "album",
              uri: "spotify:album:18Scpsg5OV1iYNtSaCsjwz"
            },
            artists: [
              {
                external_urls: {
                  spotify: "https://open.spotify.com/artist/1EpyA68dKpjf7jXmQL88Hy"
                },
                href: "https://api.spotify.com/v1/artists/1EpyA68dKpjf7jXmQL88Hy",
                id: "1EpyA68dKpjf7jXmQL88Hy",
                name: "Noname",
                type: "artist",
                uri: "spotify:artist:1EpyA68dKpjf7jXmQL88Hy"
              },
              {
                external_urls: {
                  spotify: "https://open.spotify.com/artist/4jqFe1fd5uul2XSSxsRvbZ"
                },
                href: "https://api.spotify.com/v1/artists/4jqFe1fd5uul2XSSxsRvbZ",
                id: "4jqFe1fd5uul2XSSxsRvbZ",
                name: "Cam O'bi",
                type: "artist",
                uri: "spotify:artist:4jqFe1fd5uul2XSSxsRvbZ"
              },
              {
                external_urls: {
                  spotify: "https://open.spotify.com/artist/2PU4qFehXQF7WnlFsJpBiJ"
                },
                href: "https://api.spotify.com/v1/artists/2PU4qFehXQF7WnlFsJpBiJ",
                id: "2PU4qFehXQF7WnlFsJpBiJ",
                name: "Raury",
                type: "artist",
                uri: "spotify:artist:2PU4qFehXQF7WnlFsJpBiJ"
              }
            ],
            available_markets: [
              'GB'
            ],
            disc_number: 1,
            duration_ms: 208000,
            episode: false,
            explicit: true,
            external_ids: {
              isrc: "TCACR1605427"
            },
            external_urls: {
              spotify: "https://open.spotify.com/track/6JvfBzqZmSiEG5MjM7OcSY"
            },
            href: "https://api.spotify.com/v1/tracks/6JvfBzqZmSiEG5MjM7OcSY",
            id: "6JvfBzqZmSiEG5MjM7OcSY",
            is_local: false,
            name: "Diddy Bop (feat. Cam O'bi & Raury)",
            popularity: 58,
            preview_url: "https://p.scdn.co/mp3-preview/eb2e71e9ad3db481ece10a39b0ecab113070daf4?cid=774b29d4f13844c495f206cafdad9c86",
            track: true,
            track_number: 3,
            type: "track",
            uri: "spotify:track:6JvfBzqZmSiEG5MjM7OcSY"
          },
          video_thumbnail: {
            url: null
          }
        },
      ],
      limit: 5,
      next: "https://api.spotify.com/v1/playlists/4Cca115KKLWcnRTKqRq5wj/tracks?offset=5&limit=5",
      offset: 0,
      previous: null,
      total: 10
    },
    type: "playlist",
    uri: "spotify:playlist:4Cca115KKLWcnRTKqRq5wj"
  }
}
