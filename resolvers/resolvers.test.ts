import { Spotify } from '../@types/SpotifyObjects';
import { SpotifyApi } from '../datasources/spotify-api';
import { playlistResponse, playlistTracksResponse } from '../testData/playlist';
import { resolvers } from './resolvers';

jest.mock("../datasources/spotify-api");

describe('Spotify resolvers', () => {
  describe('SpotifyPlaylist resolvers', () => {
    let playlist: Spotify.PlaylistObject
    let playlistTracks: Spotify.PagingObject<Spotify.PlaylistTrackObject>;
    beforeEach(() => {
      playlist = playlistResponse();
      playlistTracks = playlistTracksResponse()
    })

    const mockedSpotifyData = SpotifyApi as jest.MockedClass<typeof SpotifyApi>;

    mockedSpotifyData.mockImplementation(() => {
      return {
        getPlaylistTracks: (url, boolean) => {
          return Promise.resolve(playlistTracks);
        }
      } as SpotifyApi
    });


    it('Track_count returns total', () => {
      var expectedValue = playlist.tracks.total;

      expect(resolvers.SpotifyPlaylist.track_count(playlist)).toBe(expectedValue);
    });

    it('No next url maps response to tracks', async () => {
      const spotifyApi = new mockedSpotifyData();
      // need to do a deep clone here!
      const playlistWithNoNext = playlist;
      playlistWithNoNext.tracks.next = null;
      const expectedTracks = playlistWithNoNext.tracks.items.map(t => t.track);

      const datasource = { dataSources: { spotifyApi } };
      const actualTracks = await resolvers.SpotifyPlaylist.tracks(playlistWithNoNext, {}, datasource);

      expect(actualTracks).toEqual(expectedTracks);
    });

    it('With valid next url adds tracks to list', async () => {
      const spotifyApi = new mockedSpotifyData();
      const playlistWithNext = playlist;
      const extraTracks = playlistTracks.items.map(el => el.track);

      const expectedTracks = playlistWithNext.tracks.items.map(el => el.track);
      expectedTracks.push(...extraTracks);

      const datasource = { dataSources: { spotifyApi } };
      const actualTracks = await resolvers.SpotifyPlaylist.tracks(playlistWithNext, {}, datasource);

      expect(actualTracks).toEqual(expectedTracks);
    })
  });
});