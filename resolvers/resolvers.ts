import { ISpotifyApi } from '../datasources/spotify-api';
import { Spotify } from '../@types/SpotifyObjects';

interface SpotifyDataSource {
  dataSources: {
    spotifyApi: ISpotifyApi;
  }
}


export const resolvers = {
  Query: {
    playlist: (_: any, { id }: { id: string }, { dataSources }: SpotifyDataSource) => {
      return dataSources.spotifyApi.getPlaylist(id);
    },
    song: (_: any, { id }: { id: string }, { dataSources }: SpotifyDataSource) => {
      return dataSources.spotifyApi.getTrack(id);
    },
    audioFeatures: (_: any, { id }: { id: string }, { dataSources }: SpotifyDataSource) => {
      return dataSources.spotifyApi.getAudioFeatures(id);
    }

  },

  SpotifyPlaylist: {
    async tracks(parent: Spotify.PlaylistObject, { }, { dataSources }: SpotifyDataSource) {
      let response = parent.tracks.items.map((el) => el.track);
      let nextUrl = parent.tracks.next;
      while (nextUrl !== null) {
        let tempRes = await dataSources.spotifyApi.getPlaylistTracks(nextUrl, true);
        response = [...response, ...tempRes.items.map((el) => el.track)];
        nextUrl = tempRes.next;
      }
      return response;
    },

    track_count(parent: Spotify.PlaylistObject) {
      return parent.tracks.total;
    }
  },

  SpotifySong: {
    audio_features: (parent: Spotify.TrackObject, { }, { dataSources }: SpotifyDataSource) => {
      return dataSources.spotifyApi.getAudioFeatures(parent.id);
    }
  }
}
